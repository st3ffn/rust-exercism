use crate::Classification::{Abundant, Deficient, Perfect};
use std::cmp::Ordering;

#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient,
}

pub fn classify(num: u64) -> Option<Classification> {
    match (1..=num / 2)
        .filter(|&x| num % x == 0)
        .sum::<u64>()
        .cmp(&num)
    {
        _ if num == 0 => None,
        Ordering::Equal => Some(Perfect),
        Ordering::Greater => Some(Abundant),
        Ordering::Less => Some(Deficient),
    }
}
