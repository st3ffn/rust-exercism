#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::collections::HashMap;

/// Count occurrences of words.
pub fn word_count(words: &str) -> HashMap<String, u32> {
    lazy_static! {
        /// matches all word chars or apostroph 1 one or more times within a word boundary
        static ref WORDS_REGEX : Regex = Regex::new(r"\b[\w']+\b").unwrap();
    }
    WORDS_REGEX
        .find_iter(words)
        .map(|m| m.as_str().to_lowercase())
        .fold(HashMap::new(), |mut map, word| {
            *map.entry(word).or_insert(0) += 1;
            map
        })
}
