use peekmore::PeekMore;
use Error::*;
use Frame::*;

#[derive(Debug, PartialEq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

#[derive(Debug, Copy, Clone)]
enum Frame {
    Open(u16, u16),
    Spare(u16, u16),
    Strike,
    Last(u16, u16, u16),
}

impl Frame {
    fn first(self) -> u16 {
        match self {
            Open(a, _) => a,
            Spare(a, _) => a,
            Strike => 10,
            Last(a, _, _) => a,
        }
    }

    fn second(self) -> u16 {
        match self {
            Open(_, b) => b,
            Spare(_, b) => b,
            Strike => 10,
            Last(_, b, _) => b,
        }
    }
}

trait FrameBuilderBehavior {
    fn add(&mut self, pins: u16) -> Result<(), Error>;
    fn is_complete(&self) -> bool;
    fn build(&self) -> Frame;
}

#[derive(Debug, Default)]
struct NormalFrameBuilder {
    frames: Vec<u16>,
    complete: bool,
}

impl NormalFrameBuilder {
    fn new() -> Self {
        NormalFrameBuilder {
            frames: Vec::with_capacity(2),
            ..Default::default()
        }
    }
}

impl FrameBuilderBehavior for NormalFrameBuilder {
    fn add(&mut self, pins: u16) -> Result<(), Error> {
        if self.frames.get(0).unwrap_or(&0) + pins > 10 {
            return Err(NotEnoughPinsLeft);
        }
        self.complete = match (self.frames.get(0), pins) {
            // strike
            (None, pins) if pins == 10 => true,
            // open or spare
            (Some(_), _) => true,
            _ => false,
        };
        self.frames.push(pins);
        Ok(())
    }

    fn is_complete(&self) -> bool {
        self.complete
    }

    fn build(&self) -> Frame {
        match (self.frames.get(0), self.frames.get(1)) {
            (Some(_), None) => Strike,
            (Some(first), Some(second)) if first + second == 10 => Spare(*first, *second),
            (Some(first), Some(second)) => Open(*first, *second),
            _ => panic!("unsupported scenario"),
        }
    }
}

#[derive(Debug, Default)]
struct LastFrameBuilder {
    frames: Vec<u16>,
    complete: bool,
}

impl LastFrameBuilder {
    fn new() -> Self {
        LastFrameBuilder {
            frames: Vec::with_capacity(3),
            ..Default::default()
        }
    }
}

impl FrameBuilderBehavior for LastFrameBuilder {
    fn add(&mut self, pins: u16) -> Result<(), Error> {
        if pins > 10 {
            return Err(NotEnoughPinsLeft);
        }
        let balls_thrown = self.frames.len();

        if balls_thrown == 1 {
            match (self.frames[0], pins) {
                // strike
                (10, _) => (),
                // spare
                (first, second) if first + second == 10 => (),
                // invalid
                (first, second) if first + second > 10 => return Err(NotEnoughPinsLeft),
                _ => self.complete = true,
            }
        } else if balls_thrown == 2 {
            // first was strike but second not, so second and third needs to be smaller than 10
            if self.frames[0] == 10 && self.frames[1] != 10 && self.frames[1] + pins > 10 {
                return Err(NotEnoughPinsLeft);
            }
            self.complete = true
        }
        self.frames.push(pins);
        Ok(())
    }

    fn is_complete(&self) -> bool {
        self.complete
    }

    fn build(&self) -> Frame {
        Last(
            self.frames[0],
            self.frames[1],
            *self.frames.get(2).unwrap_or(&0),
        )
    }
}

fn make_frame_builder(current_frame: usize) -> Box<dyn FrameBuilderBehavior> {
    match current_frame {
        9 => Box::new(LastFrameBuilder::new()),
        _ => Box::new(NormalFrameBuilder::new())
    }
}

pub struct BowlingGame {
    builder: Box<dyn FrameBuilderBehavior>,
    frames: Vec<Frame>,
}

impl BowlingGame {

    pub fn new() -> Self {
        Self {
            builder: make_frame_builder(0),
            frames: Vec::new(),
        }
    }

    fn game_complete(&self) -> bool {
        self.frames.len() == 10
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        if self.game_complete() {
            return Err(GameComplete);
        }

        let res = self.builder.add(pins);
        if res.is_err() {
            return res;
        }
        if self.builder.is_complete() {
            self.frames.push(self.builder.build());
            self.builder = make_frame_builder(self.frames.len());
        }
        Ok(())
    }

    pub fn score(&self) -> Option<u16> {
        if self.game_complete() {
            let mut iter = self.frames.iter().peekmore();
            let mut score = 0;

            while let Some(frame) = iter.next() {
                score += match *frame {
                    Open(first, second) => first + second,
                    Last(first, second, third) => first + second + third,
                    Spare(first, second) => first + second + iter.peek().unwrap().first(),
                    Strike => {
                        let next_frame = iter.peek().unwrap();
                        match next_frame {
                            Strike => {
                                // peek even one further
                                iter.move_next();
                                let last = iter.peek().unwrap().first();
                                iter.reset_view();
                                10 + 10 + last
                            }
                            _ => 10 + next_frame.first() + next_frame.second(),
                        }
                    }
                };
            }
            return Some(score);
        }
        None
    }
}
