extern crate rand;

use rand::Rng;

pub fn encode(key: &str, s: &str) -> Option<String> {
    transform(key, s, shift_right)
}

pub fn decode(key: &str, s: &str) -> Option<String> {
    transform(key, s, shift_left)
}

pub fn encode_random(s: &str) -> (String, String) {
    let mut rng = rand::thread_rng();
    let key: String = (0..=100)
        .map(|_| rng.gen_range(b'a', b'z' + 1) as char)
        .collect();
    let encoded = encode(&key, s);
    (key, encoded.unwrap())
}

fn is_key_valid(key: &str) -> bool {
    !key.is_empty()
        && key.chars().all(|c| match c {
        'a'..='z' => true,
        _ => false,
    })
}

fn transform(key: &str, s: &str, shift: fn(char, u8) -> char) -> Option<String> {
    if is_key_valid(key) {
        Some(
            s.chars()
                .zip(key.chars().cycle())
                .map(|(c, k)| shift(c, delta(k)))
                .collect()
        )
    } else {
        None
    }
}

fn shift_right(from: char, d: u8) -> char {
    let norm = (delta(from) + d) % 26;
    (b'a' + norm) as char
}

fn shift_left(from: char, d: u8) -> char {
    shift_right('a', 26 + delta(from) - d as u8)
}

fn delta(from: char) -> u8 {
    from as u8 - b'a'
}
