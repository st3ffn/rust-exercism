pub fn factors(n: u64) -> Vec<u64> {
    if n <= 1 {
        return Vec::new();
    }

    let mut res: Vec<u64> = Vec::with_capacity(20);

    let mut divisor = 2;
    let mut current = n;

    while current != 1 {
        if current % divisor == 0 {
            current = current / divisor;
            res.push(divisor);
            continue;
        }
        
        divisor += 1;
    }

    res
}
