fn main() {
    for divisor in (1..10).rev() {
        println!("{}", divisor)
    }
}
