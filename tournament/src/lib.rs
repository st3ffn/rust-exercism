use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Default)]
struct TeamStats {
    team: String,
    won: usize,
    lost: usize,
    drawn: usize,
}

impl TeamStats {
    fn new(team: &str) -> Self {
        Self {
            team: team.to_string(),
            ..Default::default()
        }
    }

    fn game_won(&mut self) {
        self.won += 1;
    }

    fn game_lost(&mut self) {
        self.lost += 1;
    }

    fn game_drawn(&mut self) {
        self.drawn += 1;
    }

    fn played(&self) -> usize {
        self.won + self.drawn + self.lost
    }

    fn points(&self) -> usize {
        self.won * 3 + self.drawn
    }
}

pub fn tally(match_results: &str) -> String {
    let mut map: HashMap<&str, TeamStats> = HashMap::new();

    for line in match_results.lines() {
        let parts: Vec<&str> = line.split(';').collect();
        let challenger = parts[0];
        let opponent = parts[1];
        match parts[2] {
            "win" => {
                map.entry(challenger)
                    .or_insert_with(|| TeamStats::new(challenger))
                    .game_won();
                map.entry(opponent)
                    .or_insert_with(|| TeamStats::new(opponent))
                    .game_lost();
            }
            "loss" => {
                map.entry(challenger)
                    .or_insert_with(|| TeamStats::new(challenger))
                    .game_lost();
                map.entry(opponent)
                    .or_insert_with(|| TeamStats::new(opponent))
                    .game_won();
            }
            "draw" => {
                map.entry(challenger)
                    .or_insert_with(|| TeamStats::new(challenger))
                    .game_drawn();
                map.entry(opponent)
                    .or_insert_with(|| TeamStats::new(opponent))
                    .game_drawn();
            }
            _ => panic!("Unsupported match outcome in line {}", line),
        };
    }

    let mut overall_stats: Vec<&TeamStats> = map.values().collect();
    overall_stats.sort_by(|a, b| match a.points().cmp(&b.points()).reverse() {
        Ordering::Equal => a.team.cmp(&b.team),
        other => other,
    });

    let mut output = "Team                           | MP |  W |  D |  L |  P".to_string();
    for s in overall_stats {
        output.push_str(&format!(
            "\n{:<30} | {:>2} | {:>2} | {:>2} | {:>2} | {:>2}",
            s.team,
            s.played(),
            s.won,
            s.drawn,
            s.lost,
            s.points()
        ));
    }

    output
}
