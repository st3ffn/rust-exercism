#[derive(Debug, PartialEq)]
pub struct ChessPosition(isize, isize);

#[derive(Debug)]
pub struct Queen {
    pos: ChessPosition,
}

impl ChessPosition {
    pub fn new(rank: isize, file: isize) -> Option<Self> {
        match (rank, file) {
            (0..=7, 0..=7) => Some(Self(rank, file)),
            _ => None,
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self {
        Self { pos: position }
    }

    fn same_rank(&self, other: &Queen) -> bool {
        self.pos.0 == other.pos.0
    }

    fn same_file(&self, other: &Queen) -> bool {
        self.pos.1 == other.pos.1
    }

    fn is_diagonal(&self, other: &Queen) -> bool {
        (self.pos.0 - other.pos.0).abs() == (self.pos.1 - other.pos.1).abs()
    }

    pub fn can_attack(&self, other: &Queen) -> bool {
        self.same_rank(other) || self.same_file(other) || self.is_diagonal(other)
    }
}
