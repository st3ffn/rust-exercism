pub fn is_armstrong_number(num: u32) -> bool {
    let num_as_str = num.to_string();
    // can ignore unicode graphemes because we only have numbers
    let len = num_as_str.len() as u32;

    let sum: u32 = num_as_str.chars()
        .map(|c| c.to_digit(10).unwrap())
        .map(|x| x.pow(len))
        .sum();

    sum == num
}
