use std::collections::HashSet;
use std::ops::RangeInclusive;

static LOWER_A_TO_Z: RangeInclusive<char> = 'a'..='z';

/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    let set = sentence
        .to_lowercase()
        .chars()
        .filter(|c| LOWER_A_TO_Z.contains(c))
        .collect::<HashSet<_>>();
    set.len() == 26
}
