#[derive(Debug, PartialEq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

/// Convert a number between two bases.
pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    match (number.iter().find(|&&x| x >= from_base), from_base, to_base) {
        (Some(&x), _, _) => return Err(Error::InvalidDigit(x)),
        (None, 0..=1, _) => return Err(Error::InvalidInputBase),
        (None, _, 0..=1) => return Err(Error::InvalidOutputBase),
        _ => {}
    }

    let mut value = number.iter().fold(0, |acc, x| acc * from_base + x);

    let mut res = Vec::new();
    while value != 0 {
        res.push(value % to_base);
        value /= to_base;
    }
    res.reverse();

    Ok(res)
}
