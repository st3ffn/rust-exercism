macro_rules! planet {
    ($planet_name:ident, $val:expr) => {
        pub struct $planet_name;

        impl Planet for $planet_name {
            const EARTH_ORBITAL_SCALE: f64 = $val;
        }
    };
}

#[derive(Debug)]
pub struct Duration(f64);

impl From<u64> for Duration {
    fn from(s: u64) -> Self {
        Self(s as f64)
    }
}

pub trait Planet {
    const EARTH_ORBITAL_PERIOD: f64 = 31_557_600.0;
    const EARTH_ORBITAL_SCALE: f64;

    fn years_during(d: &Duration) -> f64 {
        d.0 / (Self::EARTH_ORBITAL_PERIOD * Self::EARTH_ORBITAL_SCALE)
    }
}

planet!(Earth, 1.0);
planet!(Mercury, 0.240_846_7);
planet!(Venus, 0.615_197_26);
planet!(Mars, 1.880_815_8);
planet!(Jupiter, 11.862_615);
planet!(Saturn, 29.447_498);
planet!(Uranus, 84.016_846);
planet!(Neptune, 164.79132);
