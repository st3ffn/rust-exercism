use std::collections::HashMap;

#[derive(Eq, PartialEq, Default)]
pub struct Graph {
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
    pub attrs: HashMap<String, String>,
}

impl Graph {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_nodes(mut self, nodes: &[Node]) -> Self {
        self.nodes = nodes.to_vec();
        self
    }

    pub fn with_edges(mut self, edges: &[Edge]) -> Self {
        self.edges = edges.to_vec();
        self
    }

    pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
        self.attrs = attrs
            .iter()
            .map(|&(k, v)| (k.to_string(), v.to_string()))
            .collect();
        self
    }

    pub fn get_node(&self, name: &str) -> Option<&Node> {
        self.nodes.iter().find(|node| node.name == name)
    }
}

#[derive(Eq, PartialEq, Default, Clone, Debug)]
pub struct Edge {
    src: String,
    dst: String,
    attrs: HashMap<String, String>,
}

impl Edge {
    pub fn new(src: &str, dst: &str) -> Self {
        Edge {
            src: src.to_string(),
            dst: dst.to_string(),
            ..Self::default()
        }
    }

    pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
        self.attrs = attrs
            .iter()
            .map(|&(k, v)| (k.to_string(), v.to_string()))
            .collect();
        self
    }
}

#[derive(Eq, PartialEq, Default, Clone, Debug)]
pub struct Node {
    name: String,
    attrs: HashMap<String, String>,
}

impl Node {
    pub fn new(name: &str) -> Self {
        Node {
            name: name.to_string(),
            ..Self::default()
        }
    }

    pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
        self.attrs = attrs
            .iter()
            .map(|&(k, v)| (k.to_string(), v.to_string()))
            .collect();
        self
    }

    pub fn get_attr(&self, key: &str) -> Option<&str> {
        self.attrs.get(key).map(|v| v.as_str())
    }
}

pub mod graph {
    pub use super::Graph;
    pub mod graph_items {
        pub mod node {
            pub use super::super::super::Node;
        }

        pub mod edge {
            pub use super::super::super::Edge;
        }
    }
}
