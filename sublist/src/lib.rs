use Comparison::*;

#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(first: &[T], second: &[T]) -> Comparison {
    match (first, first.len(), second, second.len()) {
        (a, _, b, _) if a == b => Equal,
        (_, a_len, _, _) if a_len == 0 => Sublist,
        (_, _, _, b_len) if b_len == 0 => Superlist,
        (a, _, b, _) if b.windows(a.len()).any(|slice| slice == a) => Sublist,
        (a, _, b, _) if a.windows(b.len()).any(|slice| slice == b) => Superlist,
        _ => Unequal
    }
}
