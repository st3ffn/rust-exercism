use std::collections::BTreeMap;

pub fn transform(input: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    input
        .iter()
        .flat_map(|(points, chars)| chars.iter().map(move |c| (to_lowercase(*c), *points)))
        .collect()
}

fn to_lowercase(c: char) -> char {
    c.to_lowercase().collect::<Vec<_>>()[0]
}
