use std::collections::HashSet;

pub fn check(candidate: &str) -> bool {
    let set: HashSet<_> = candidate.to_lowercase().chars().filter(|c| c.is_alphabetic()).collect();
    set.len() == candidate.to_lowercase().chars().filter(|c| c.is_alphabetic()).count()
}
