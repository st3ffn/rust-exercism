use std::char;

pub fn annotate(minefield: &[&str]) -> Vec<String> {
    let mut res = Vec::new();

    for (r_i, row) in minefield.iter().enumerate() {
        let mut res_r = Vec::new();

        for (c_i, c) in row.chars().enumerate() {
            match c {
                '*' => res_r.push('*'),
                _ => {
                    let mut score = 0;
                    if let Some(c) = up(minefield, r_i, c_i) {
                        if c == '*' {
                            score += 1;
                        }
                        // up left
                        if is_star(left(minefield, r_i - 1, c_i)) {
                            score += 1;
                        }
                        // up right
                        if is_star(right(minefield, r_i - 1, c_i)) {
                            score += 1;
                        }
                    }
                    if let Some(c) = down(minefield, r_i, c_i) {
                        if c == '*' {
                            score += 1;
                        }
                        // down left
                        if is_star(left(minefield, r_i + 1, c_i)) {
                            score += 1;
                        }
                        // down right
                        if is_star(right(minefield, r_i + 1, c_i)) {
                            score += 1;
                        }
                    }
                    if is_star(left(minefield, r_i, c_i)) {
                        score += 1;
                    }
                    if is_star(right(minefield, r_i, c_i)) {
                        score += 1;
                    }
                    let c = match score {
                        0 => ' ',
                        _ => char::from_digit(score, 10).unwrap(),
                    };
                    res_r.push(c);
                }
            }
        }
        res.push(res_r.iter().collect())
    }
    res
}
fn is_star(opt: Option<char>) -> bool {
    match opt {
        Some(c) if c == '*' => true,
        _ => false,
    }
}

fn up(source: &[&str], row: usize, col: usize) -> Option<char> {
    match row.checked_sub(1) {
        Some(r) => source[r].chars().nth(col),
        None => None,
    }
}

fn down(source: &[&str], row: usize, col: usize) -> Option<char> {
    if source.len() > row + 1 {
        return source[row + 1].chars().nth(col);
    }
    None
}

fn left(source: &[&str], row: usize, col: usize) -> Option<char> {
    match col.checked_sub(1) {
        Some(c) => source[row].chars().nth(c),
        None => None,
    }
}

fn right(source: &[&str], row: usize, col: usize) -> Option<char> {
    source[row].chars().nth(col + 1)
}
