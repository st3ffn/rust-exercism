use std::iter;

#[derive(Debug)]
pub struct PascalsTriangle {
    row_count: u32,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        PascalsTriangle { row_count }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        let mut res: Vec<Vec<u32>> = Vec::new();

        for _ in 1..=self.row_count {
            let row = match res.last() {
                Some(last) => iter::once(1)
                    .chain(last.windows(2).map(|s| s[0] + s[1]))
                    .chain(iter::once(1))
                    // = [1, sum of window, ..., 1]
                    .collect(),
                None => vec![1; 1],
            };
            res.push(row);
        }
        res
    }
}
