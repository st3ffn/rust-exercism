/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let trimmed: Vec<char> = code.chars().filter(|c| !c.is_whitespace()).collect();

    if trimmed.len() <= 1 || trimmed.iter().any(|c| !c.is_numeric()) {
        return false;
    }

    trimmed
        .iter()
        .rev()
        .map(|c| c.to_digit(10).unwrap())
        .enumerate()
        .map(|(i, n)| {
            match (i % 2 == 1, n) {
                (true, n) if n > 4 => n * 2 - 9,
                (true, n) => n * 2,
                _ => n
            }
        })
        .sum::<u32>() % 10 == 0
}
