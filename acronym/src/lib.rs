pub fn abbreviate(phrase: &str) -> String {
    // add space in the beginning
    let mut s: String = String::from(" ");
    s.push_str(phrase);

    s.chars()
        .collect::<Vec<_>>()
        // take two chars
        .windows(2)
        .filter(|pair| is_candidate((pair[0], pair[1])))
        .map(|pair| pair[1])
        .collect::<String>()
        .to_uppercase()
}

pub fn is_candidate((a, b): (char, char)) -> bool {
    match (
        !a.is_alphabetic(),
        b.is_alphabetic(),
        a.is_lowercase(),
        b.is_uppercase(),
    ) {
        (true, true, _, _) => true,
        (_, _, true, true) => true,
        _ => false,
    }
}
