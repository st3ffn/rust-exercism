use std::fmt::{Display, Formatter, Result};

const _1000_TO_3000: [&str; 4] = ["", "M", "MM", "MMM"];
const _100_TO_900: [&str; 10] = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"];
const _10_TO_90: [&str; 10] = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"];
const _1_TO_9: [&str; 10] = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"];

pub struct Roman<'a> {
    thousands: &'a str,
    hundereds: &'a str,
    tens: &'a str,
    ones: &'a str,
}

impl<'a> Display for Roman<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(
            f,
            "{}{}{}{}",
            self.thousands, self.hundereds, self.tens, self.ones
        )
    }
}
impl<'a> From<u32> for Roman<'a> {
    fn from(num: u32) -> Self {
        Self {
            thousands: _1000_TO_3000[(num / 1000) as usize],
            hundereds: _100_TO_900[((num % 1000) / 100) as usize],
            tens: _10_TO_90[((num % 100) / 10) as usize],
            ones: _1_TO_9[(num % 10) as usize],
        }
    }
}
