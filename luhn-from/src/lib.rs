pub struct Luhn(String);

impl Luhn {
    pub fn is_valid(&self) -> bool {
        if self.0.chars().count() <= 1 || self.0.chars().any(|c| !c.is_numeric()) {
            return false;
        }
        self.0
            .chars()
            .rev()
            .enumerate()
            .try_fold(0u32, |acc, (i, c)| {
                match (c.to_digit(10), i % 2 == 1) {
                    (Some(num), true) if num > 4 => Some(num * 2 - 9),
                    (Some(num), true) => Some(num * 2),
                    (Some(num), _) => Some(num),
                    _ => None,
                }
                .map(|num| acc + num)
            })
            .map_or(false, |num| num % 10 == 0)
    }
}

impl<T: ToString> From<T> for Luhn {
    fn from(input: T) -> Self {
        Self(
            input
                .to_string()
                .chars()
                .filter(|c| !c.is_whitespace())
                .collect(),
        )
    }
}
