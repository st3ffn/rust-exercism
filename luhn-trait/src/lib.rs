extern crate luhn_from;

pub trait Luhn {
    fn valid_luhn(&self) -> bool;
}

impl<T: ToString> Luhn for T {
    fn valid_luhn(&self) -> bool {
        luhn_from::Luhn::from(self.to_string()).is_valid()
    }
}
