use std::fmt;

pub fn encode(n: u64) -> String {
    match n {
        0..=19 => zero_to_nineteen(n),
        20..=99 if n % 10 == 0 => ten(n / 10),
        21..=99 => format!("{}-{}", ten(n / 10), zero_to_nineteen(n % 10)),
        _ => {
            let prefix = Prefix::from(n);
            if n % prefix.value() == 0 {
                format!("{} {}", encode(n / prefix.value()), prefix)
            } else {
                format!(
                    "{} {} {}",
                    encode(n / prefix.value()),
                    prefix,
                    encode(n % prefix.value())
                )
            }
        }
    }
}

enum Prefix {
    Hundred,
    Thousand,
    Million,
    Billion,
    Trillion,
    Quadrillion,
    Quintillion,
}

impl Prefix {
    fn value(&self) -> u64 {
        match self {
            Prefix::Hundred => 100,
            Prefix::Thousand => 1_000,
            Prefix::Million => 1_000_000,
            Prefix::Billion => 1_000_000_000,
            Prefix::Trillion => 1_000_000_000_000,
            Prefix::Quadrillion => 1_000_000_000_000_000,
            Prefix::Quintillion => 1_000_000_000_000_000_000,
        }
    }
}

impl From<u64> for Prefix {
    fn from(n: u64) -> Self {
        match n {
            100..=999 => Prefix::Hundred,
            1_000..=999_999 => Prefix::Thousand,
            1_000_000..=999_999_999 => Prefix::Million,
            1_000_000_000..=999_999_999_999 => Prefix::Billion,
            1_000_000_000_000..=999_999_999_999_999 => Prefix::Trillion,
            1_000_000_000_000_000..=999_999_999_999_999_999 => Prefix::Quadrillion,
            1_000_000_000_000_000_000..=std::u64::MAX => Prefix::Quintillion,
            _ => panic!("valid for numbers greater or equal 100 but was {}", n),
        }
    }
}

impl fmt::Display for Prefix {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Prefix::Hundred => write!(f, "hundred"),
            Prefix::Thousand => write!(f, "thousand"),
            Prefix::Million => write!(f, "million"),
            Prefix::Billion => write!(f, "billion"),
            Prefix::Trillion => write!(f, "trillion"),
            Prefix::Quadrillion => write!(f, "quadrillion"),
            Prefix::Quintillion => write!(f, "quintillion"),
        }
    }
}

fn ten(n: u64) -> String {
    match n {
        2 => "twenty",
        3 => "thirty",
        4 => "forty",
        5 => "fifty",
        6 => "sixty",
        7 => "seventy",
        8 => "eighty",
        9 => "ninety",
        _ => panic!("valid for numbers between [2..9] but was {}", n),
    }
    .to_string()
}

fn zero_to_nineteen(n: u64) -> String {
    match n {
        0 => "zero",
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen",
        _ => panic!("valid for numbers between [0..19] but was {}", n),
    }
    .to_string()
}
