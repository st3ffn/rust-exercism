use chrono::{Duration, NaiveTime};
use std::cmp::PartialEq;
use std::fmt;

#[derive(Debug)]
pub struct Clock {
    hours: i64,
    minutes: i64,
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let time = NaiveTime::from_hms(0, 0, 0)
            + Duration::hours(self.hours)
            + Duration::minutes(self.minutes);
        write!(f, "{}", time.format("%H:%M"))
    }
}

impl PartialEq for Clock {
    fn eq(&self, other: &Self) -> bool {
        self.to_string() == other.to_string()
    }
}

impl Clock {
    pub fn new(hours: i64, minutes: i64) -> Self {
        Clock { hours, minutes }
    }

    pub fn add_minutes(&self, minutes: i64) -> Self {
        Clock::new(self.hours, self.minutes + minutes)
    }
}
