use std::iter::FromIterator;
use std::mem;

#[derive(Debug)]
pub struct SimpleLinkedList<T> {
    head: Option<Box<Node<T>>>,
}

#[derive(Debug)]
struct Node<T> {
    data: T,
    next: Option<Box<Node<T>>>,
}

impl<T> Default for SimpleLinkedList<T> {
    fn default() -> Self {
        SimpleLinkedList { head: None }
    }
}

impl<T> SimpleLinkedList<T> {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn len(&self) -> usize {
        let mut c = &self.head;
        let mut len = 0;

        while c.is_some() {
            len += 1;
            c = &c.as_ref().unwrap().next;
        }
        len
    }

    pub fn is_empty(&self) -> bool {
        self.peek().is_none()
    }

    pub fn push(&mut self, _element: T) {
        // LIFO: this _element will be the new head
        self.head = Some(Box::new(Node {
            data: _element,
            // it's like self.head.getAndSet(None)
            // equivalent to self.head.take()
            next: mem::replace(&mut self.head, None),
        }));
    }

    pub fn pop(&mut self) -> Option<T> {
        // LIFO: head will be replaced
        // it's like self.head.getAndSet(None)
        // equivalent to mem::replace(&mut self.head, None)
        let old_head: Option<Box<Node<T>>> = self.head.take();
        match old_head {
            Some(node) => {
                self.head = node.next;
                Some(node.data)
            }
            _ => None,
        }
    }

    pub fn peek(&self) -> Option<&T> {
        match &self.head {
            Some(node) => Some(&node.data),
            _ => None,
        }
    }

    pub fn rev(self) -> SimpleLinkedList<T> {
        // self is LIFO, we want FIFO
        // if we push the elements in order, the first element will be the last
        let mut reversed = SimpleLinkedList::new();

        let mut current = self.head;
        while let Some(node) = current {
            reversed.push(node.data);
            current = node.next;
        }
        reversed
    }
}

impl<T> FromIterator<T> for SimpleLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(_iter: I) -> Self {
        let mut list = SimpleLinkedList::new();
        for x in _iter {
            list.push(x);
        }
        list
    }
}

impl<T> Into<Vec<T>> for SimpleLinkedList<T> {
    fn into(mut self) -> Vec<T> {
        let mut v = vec![];

        while let Some(x) = self.pop() {
            // self is LIFO but FIFO is expected
            v.insert(0, x);
        }
        v
    }
}
