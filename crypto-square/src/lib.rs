pub fn encrypt(input: &str) -> String {
    let normalized: Vec<char> = input
        .chars()
        .filter(|&c| c.is_alphabetic())
        .map(|c| c.to_ascii_lowercase())
        .collect();

    let c = (normalized.len() as f64).sqrt().ceil() as usize;

    (0..c)
        .map(|i| {
            (0..c)
                .filter_map(|j| normalized.get(i + j * c))
                .collect::<String>()
        })
        .collect::<Vec<String>>()
        .join(" ")
}
