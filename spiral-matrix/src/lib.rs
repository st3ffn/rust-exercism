pub fn spiral_matrix(size: usize) -> Vec<Vec<u32>> {
    let mut res: Vec<Vec<u32>> = (1..=size).map(|_| vec![0; size]).collect();

    let mut value = 0;
    let mut row: i32 = 0;
    let mut col: i32 = 0;

    for i in 0..size {
        if i % 2 == 0 {
            for _ in 0..(size - i) {
                value += 1;
                res[row as usize][col as usize] = value;
                col += 1;
            }
            col -= 1;
            row += 1;

            for _ in 1..(size - i) {
                value += 1;
                res[row as usize][col as usize] = value;
                row += 1;
            }
            row -= 1;
            col -= 1;
        } else {
            for _ in 0..(size - i) {
                value += 1;
                res[row as usize][col as usize] = value;
                col -= 1;
            }
            col += 1;
            row -= 1;

            for _ in 1..(size - i) {
                value += 1;
                res[row as usize][col as usize] = value;
                row -= 1;
            }
            row += 1;
            col += 1;
        }
    }
    res
}
