static TABLE: [char; 26] = [
    'z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h',
    'g', 'f', 'e', 'd', 'c', 'b', 'a',
];
/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    decode_str(plain)
        .chunks(5)
        .map(|c| c.iter().collect::<String>())
        .collect::<Vec<String>>()
        .join(" ")
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    decode_str(cipher).iter().collect()
}

fn decode_str(cipher: &str) -> Vec<char> {
    cipher
        .chars()
        .filter(|c| c.is_alphanumeric())
        .map(|c| {
            if c.is_alphabetic() {
                TABLE[c.to_ascii_lowercase() as usize - 97]
            } else {
                c.to_ascii_lowercase()
            }
        })
        .collect()
}
