#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

pub fn number(user_number: &str) -> Option<String> {
    Some(
        user_number
            .chars()
            .filter(|c| c.is_ascii_digit())
            .collect::<String>(),
    )
    .map(|numbers| extract_number(&numbers))
    .unwrap()
}

fn extract_number(numbers: &str) -> Option<String> {
    lazy_static! {
        // START with optional 1 then digit [2-9] then two digits
        // then digit [2-9] then 6 digits END
        // all digits are grouped
        // e.g. 2234567890 or 12234567890, g1 = 2234567890
        static ref NANP_NUMBER: Regex = Regex::new(r"^1?([2-9]\d{2}[2-9]\d{6})$").unwrap();
    }
    match NANP_NUMBER.captures(numbers) {
        Some(caps) => caps.get(1).map(|m| m.as_str().to_string()).or(None),
        _ => None,
    }
}
