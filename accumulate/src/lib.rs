pub fn map<T, R, F>(input: Vec<T>, f: F) -> Vec<R>
where
    F: FnMut(T) -> R,
{
    input.into_iter().map(f).collect()
}
