use std::collections::HashSet;

struct Container<'a> {
    input: &'a str,
    lower: String,
}

impl<'a> Container<'a> {
    fn new(input: &'a str) -> Container {
        Container {
            input: input,
            lower: input.to_lowercase(),
        }
    }

    fn uniode_sum(&self) -> u32 {
        self.lower.chars().map(|c| c as u32).sum()
    }

    fn is_anagram_of(&self, other: &Container) -> bool {
        self.lower != other.lower && self.uniode_sum() == other.uniode_sum()
    }
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &'a [&str]) -> HashSet<&'a str> {
    possible_anagrams
        .iter()
        .map(|ana| Container::new(ana))
        .filter(|container| container.is_anagram_of(&Container::new(word)))
        .map(|container| container.input)
        .collect()
}
