use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        panic!("Unsigned integer as parameter is required");
    }

    let s = &args[1].parse::<u32>().unwrap();

    println!("On field {} are {} grains", s, square(s));
}

fn square(s: &u32) -> u64 {
    1 << (s-1)
}