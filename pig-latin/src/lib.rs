#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

lazy_static! {
    static ref PIG_LATIN_PATTERN: Regex = Regex::new(
        r"(?x)
            (?P<vowel>[xy][bcdfghjklmnpqrstvwxz]+)?
            (?P<consonant>y|(qu|[bcdfghjklmnpqrstvwxz])+)?
            (?P<leftover>.*)
        ",
    )
    .unwrap();
}

pub fn translate(input: &str) -> String {
    input
        .split_whitespace()
        .map(|word| translate_word(word))
        .collect::<Vec<_>>()
        .join(" ")
}

fn translate_word(word: &str) -> String {
    PIG_LATIN_PATTERN
        .replace(word, "${vowel}${leftover}${consonant}ay")
        .to_string()
}
