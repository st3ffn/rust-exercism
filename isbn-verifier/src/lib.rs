#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    match extract(&isbn) {
        Some(digits) => validate(&digits),
        _ => false,
    }
}

fn extract(isbn_numbers: &str) -> Option<String> {
    lazy_static! {
        // starts with 1 digit then optional '-' then 3 digits then optional '-'
        // then 5 digits then optional '-' and ends with digit OR 'X'.
        // all digits are grouped
        // e.g. 3-598-21507-X or 359821507X, g1 = 3, g2 = 598, g3 = 21507, g4 = X
        static ref ISBN_10: Regex = Regex::new(r"^(\d)-?(\d{3})-?(\d{5})-?([\d|X])$").unwrap();
    }
    match ISBN_10.captures(isbn_numbers) {
        Some(caps) => Some(
            caps.iter()
                .skip(1)
                .filter_map(|cap| cap.map(|m| m.as_str()))
                .collect(),
        ),
        _ => None,
    }
}

fn validate(isbn: &str) -> bool {
    let value = (1..=10).rev().fold(0, |acc, i| {
        acc + isbn.chars().nth(i - 1).unwrap().to_digit(10).unwrap_or(10) * i as u32
    });

    value % 11 == 0
}
