pub fn raindrops(n: u32) -> String {
    // clojure 
    let is_factor = |factor| n % factor == 0 ;
    let mut rez = String::new();

    if is_factor(3) { rez += "Pling"; }
    if is_factor(5) { rez += "Plang"; }
    if is_factor(7) { rez += "Plong"; }

    if rez.is_empty() { rez = n.to_string(); }

    rez

    // let mut rez : String = String::new();
    // if n % 3 == 0 {
    //     rez += "Pling";
    // } 
    // if n % 5 == 0 {
    //     rez += "Plang";
    // }
    
    // if n % 7 == 0 {
    //     rez += "Plong";
    // } 

    // if rez.is_empty() {
    //     rez = n.to_string();
    // }
       
    // rez
}
