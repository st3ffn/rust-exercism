use std::collections::HashMap;

static NUCLEOTIDES: [char; 4] = ['A', 'C', 'G', 'T'];

fn is_nucleotide(c: char) -> Result<char, char> {
    match NUCLEOTIDES.contains(&c) {
        true => Ok(c),
        _ => Err(c),
    }
}

pub fn count(c: char, dna: &str) -> Result<usize, char> {
    is_nucleotide(c).and_then(|_| {
        dna.chars()
            .map(is_nucleotide)
            // use collect to get first result which failed
            .collect::<Result<Vec<_>, _>>()
            .map(|v| v.into_iter().filter(|x| *x == c).count())
    })
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    NUCLEOTIDES
        .iter()
        .map(|c| count(*c, dna).map(|size| (*c, size)))
        .collect()
}
