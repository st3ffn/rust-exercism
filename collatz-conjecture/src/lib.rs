pub fn collatz(n: u64) -> Option<u64> {
    let mut i = 0;
    let mut n = n;
    loop {
        n = match n {
            0 => return None,
            1 => return Some(i),
            x if x % 2 == 0 => n / 2,
            _ => 3 * n + 1,
        };
        i += 1;
    }
}
