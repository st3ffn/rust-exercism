extern crate regex;

use regex::Regex;

pub fn reply(message: &str) -> &str {
    let regex_yell_question = Regex::new(r"^([A-Z '])+\?$").unwrap();
    if regex_yell_question.is_match(message) {
        return "Calm down, I know what I'm doing!";
    }

    let regex_question = Regex::new(r"(.)+\?([ ])*$").unwrap();
    if regex_question.is_match(message) {
        return "Sure.";
    }

    let regex_nothing_to_say = Regex::new(r"^(\s)*$").unwrap();
    if regex_nothing_to_say.is_match(message) {
        return "Fine. Be that way!";
    }

    let regex_yell = Regex::new(r"([A-Z]){2,}").unwrap();
    let regex_dot_at_the_end = Regex::new(r"[.]$").unwrap();
    if regex_yell.is_match(message) && !regex_dot_at_the_end.is_match(message) {
        return "Whoa, chill out!";
    }

    "Whatever."
}
