use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use failure::Error;

#[derive(Debug, Eq, PartialEq, Hash)]
enum Flag {
    CaseInsensitive,
    MatchLine,
    Invert,
    FileName,
    LineNumber,
}

#[derive(Debug)]
pub struct Flags(HashSet<Flag>);

impl Flags {
    pub fn new(flags: &[&str]) -> Self {
        Self(
            flags
                .iter()
                .filter_map(|&flag| match flag {
                    "-i" => Some(Flag::CaseInsensitive),
                    "-x" => Some(Flag::MatchLine),
                    "-v" => Some(Flag::Invert),
                    "-l" => Some(Flag::FileName),
                    "-n" => Some(Flag::LineNumber),
                    _ => None,
                })
                .collect(),
        )
    }

    fn is_flag(&self, flag: Flag) -> bool {
        self.0.contains(&flag)
    }
}

fn match_line(line: &str, flags: &Flags, pattern: &str) -> bool {
    let pattern = if flags.is_flag(Flag::CaseInsensitive) {
        pattern.to_lowercase()
    } else {
        pattern.to_string()
    };

    let candidate = if flags.is_flag(Flag::CaseInsensitive) {
        line.to_lowercase()
    } else {
        line.to_string()
    };

    if flags.is_flag(Flag::MatchLine) {
        candidate == pattern
    } else {
        candidate.contains(&pattern)
    }
}

fn format_line(
    line: &str,
    line_number: usize,
    flags: &Flags,
    filename: &str,
    multiple_files: bool,
) -> String {
    let mut res = String::new();
    if multiple_files {
        res.push_str(&format!("{}:", filename));
    }

    if flags.is_flag(Flag::LineNumber) {
        res.push_str(&format!("{}:", line_number));
    }

    res.push_str(line);
    res
}

fn run(
    pattern: &str,
    flags: &Flags,
    filename: &str,
    multiple_files: bool,
) -> Result<Vec<String>, Error> {
    // read file line by line
    let f = BufReader::new(File::open(filename)?);
    let mut res: Vec<String> = vec![];

    for (i, line) in f.lines().enumerate() {
        let line = line?;

        let matches = if flags.is_flag(Flag::Invert) {
            !match_line(&line, flags, pattern)
        } else {
            match_line(&line, flags, pattern)
        };

        if matches {
            if flags.is_flag(Flag::FileName) {
                res.push(filename.to_string());

                return Ok(res);
            }

            res.push(format_line(&line, i + 1, flags, filename, multiple_files));
        }
    }

    Ok(res)
}

pub fn grep(pattern: &str, flags: &Flags, files: &[&str]) -> Result<Vec<String>, Error> {
    files
        .iter()
        .map(|filename| run(pattern, flags, filename, files.len() > 1))
        // actually Result<Vec<Vec<String>>, Error>
        .collect::<Result<Vec<_>, _>>()
        // flatterned to Result<Vec<String>, Error>
        .map(|r| r.into_iter().flatten().collect::<Vec<_>>())
}
