#[derive(Debug, PartialEq)]
pub enum Error {
    IncompleteNumber,
    Overflow,
}

/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    values
        .iter()
        .flat_map(|&v| {
            let mut v = v;
            let mut res: Vec<u8> = Vec::new();

            for i in 0.. {
                // (last 7 bits, is_first)
                let x = match ((v & 127) as u8, i == 0) {
                    (x, true) => x,
                    (x, false) => x | 128,
                };
                res.push(x);
                v >>= 7;
                if v == 0 {
                    break;
                }
            }
            res.into_iter().rev()
        })
        .collect()
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, Error> {
    let mut res = Vec::new();
    let mut v: u32 = 0;
    let mut last = false;

    for &b in bytes {
        // means not enough space for next
        if v.leading_zeros() < 7 {
            return Err(Error::Overflow);
        }
        last = b & 128 == 0;

        v = v << 7;
        // set last 7 bits
        v |= (b & 127) as u32;

        if last {
            res.push(v);
            v = 0;
        }
    }

    if !last {
        return Err(Error::IncompleteNumber)
    }

    Ok(res)
}
