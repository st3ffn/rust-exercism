pub fn nth(n: u32) -> u32 {
    (2..)
        .filter(self::is_prime)
        .take(n as usize + 1)
        .last()
        .unwrap()
}

fn is_prime(number: &u32) -> bool {
    if *number == 2 {
        return true;
    }

    (2..*number)
        .skip_while(|x| *number % x != 0)
        .take(1)
        .last()
        .is_none()
}
