use Allergen::*;

pub struct Allergies(u8);

const ALLERGIES: [Allergen; 8] = [
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
];

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Allergen {
    Eggs = 1,
    Peanuts = 2,
    Shellfish = 4,
    Strawberries = 8,
    Tomatoes = 16,
    Chocolate = 32,
    Pollen = 64,
    Cats = 128,
}

impl Allergen {
    fn value(self) -> u8 {
        self as u8
    }

    fn iterator() -> impl Iterator<Item = Allergen> {
        ALLERGIES.iter().copied()
    }
}

impl Allergies {
    pub fn new(score: u32) -> Self {
        // normalize the score to max 255 (sum of all allergies)
        // score as u8 is same as (score & 255)
        Self(score as u8)
    }

    pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
        self.0 & allergen.value() != 0
    }

    pub fn allergies(&self) -> Vec<Allergen> {
        Allergen::iterator()
            .filter(|a| self.is_allergic_to(a))
            .collect()
    }
}
