use core::cmp::Ordering;

pub fn find<T: Ord, V: AsRef<[T]>>(input: V, key: T) -> Option<usize> {
    let input = input.as_ref();
    let mut left = 0;
    let mut right = input.len();

    while left < right {
        let mid = (left + right) / 2;
        match key.cmp(&input[mid]) {
            Ordering::Equal => return Some(mid),
            Ordering::Less => right = mid,
            Ordering::Greater => left = mid + 1,
        }
    }

    None
}
