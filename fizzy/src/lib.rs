use std::iter;
use std::ops;

/// A Matcher is a single rule of fizzbuzz: given a function on T, should
/// a word be substituted in? If yes, which word?
pub struct Matcher<'a, T> {
    predicate: Box<dyn Fn(T) -> bool + 'a>,
    subs: String,
}

impl<'a, T> Matcher<'a, T> {
    pub fn new<F, S>(predicate: F, subs: S) -> Matcher<'a, T>
    where
        F: Fn(T) -> bool + 'a,
        S: ToString,
    {
        Self {
            predicate: Box::new(predicate),
            subs: subs.to_string(),
        }
    }

    fn substitute(&self, elem: T) -> Option<String> {
        if (self.predicate)(elem) {
            Some(self.subs.to_string())
        } else {
            None
        }
    }
}

/// A Fizzy is a set of matchers, which may be applied to an iterator.
///
/// Strictly speaking, it's usually more idiomatic to use `iter.map()` than to
/// consume an iterator with an `apply` method. Given a Fizzy instance, it's
/// pretty straightforward to construct a closure which applies it to all
/// elements of the iterator. However, we're using the `apply` pattern
/// here because it's a simpler interface for students to implement.
///
/// Also, it's a good excuse to try out using impl trait.]
pub struct Fizzy<'a, T>(Vec<Matcher<'a, T>>);

impl<'a, T> Default for Fizzy<'a, T> {
    fn default() -> Self {
        Self(Vec::new())
    }
}

impl<'a, T> Fizzy<'a, T>
where
    T: ToString + Clone + 'a,
{
    pub fn new() -> Self {
        Default::default()
    }

    pub fn add_matcher(self, matcher: Matcher<'a, T>) -> Self {
        Self(self.0.into_iter().chain(iter::once(matcher)).collect())
    }

    /// map this fizzy onto every element of an iterator, returning a new iterator
    pub fn apply<I>(self, iter: I) -> impl Iterator<Item = String> + 'a
    where
        I: Iterator<Item = T> + 'a,
    {
        iter.map(move |elem| self.apply_val(elem))
    }

    fn apply_val(&self, elem: T) -> String {
        let substitutions: Vec<String> = self
            .0
            .iter()
            .filter_map(|matcher| matcher.substitute(elem.clone()))
            .collect();
        if substitutions.is_empty() {
            elem.to_string()
        } else {
            substitutions.join("")
        }
    }
}

/// convenience function: return a Fizzy which applies the standard fizz-buzz rules
pub fn fizz_buzz<'a, T>() -> Fizzy<'a, T>
where
    T: ToString + Clone + PartialEq + From<u8> + ops::Rem<Output = T> + 'a,
{
    Fizzy::new()
        .add_matcher(Matcher::new(|x| x % T::from(3) == T::from(0), "fizz"))
        .add_matcher(Matcher::new(|x| x % T::from(5) == T::from(0), "buzz"))
}
