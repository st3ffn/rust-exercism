pub fn rotate(input: &str, key: i8) -> String {
    input
        .chars()
        .map(|c| match c {
            'a'..='z' => rotate_lowercase_letter(c, key),
            'A'..='Z' => rotate_lowercase_letter(c.to_ascii_lowercase(), key).to_ascii_uppercase(),
            _ => c,
        })
        .collect()
}

fn rotate_lowercase_letter(c: char, key: i8) -> char {
    let key = key % 26;
    // normalize from ascii 'a' = 0; 'z' = 25
    let c = c as i8 - 'a' as i8;
    // absolute letter position between [0, 25]
    let abs = (c + key) % 26;
    // back to ascii
    (b'a' + abs as u8) as char
}
