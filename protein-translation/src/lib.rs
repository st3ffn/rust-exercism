use std::collections::HashMap;

static STOP_CODON: &str = "stop codon";

// Contains Map<Codon, Name>
pub struct CodonsInfo<'a>(HashMap<&'a str, &'a str>);

impl<'a> CodonsInfo<'a> {
    pub fn name_for(&self, codon: &str) -> Option<&'a str> {
        // HashMap.get(..) will return ref to &str so Option<&&str>
        // could also be done by using match
        self.0.get(codon).copied()
    }

    /// Iterator of codons which are groups of 3 chars of the given RNA
    fn codons(&self, rna: &'a str) -> impl Iterator<Item = String> + 'a {
        let mut rna_iter = rna.chars();
        (0..)
            .map(move |_| rna_iter.by_ref().take(3).collect::<String>())
            .take_while(|s| !s.is_empty())
    }

    pub fn of_rna(&self, rna: &str) -> Option<Vec<&'a str>> {
        self.codons(rna)
            .map(|codon| self.name_for(&codon))
            .take_while(|&s| s != Some(STOP_CODON))
            // actually Option<Vec<&str>>
            .collect::<Option<Vec<_>>>()
    }
}

pub fn parse<'a>(pairs: Vec<(&'a str, &'a str)>) -> CodonsInfo<'a> {
    CodonsInfo(pairs.into_iter().collect())
}
