use std::collections::HashMap;

pub struct CodonsInfo<'a>(HashMap<&'a str, Vec<&'a str>>);

impl<'a> CodonsInfo<'a> {
    pub fn name_for(&self, codon: &str) -> Option<&'a str> {
        self.0
            .iter()
            .find(|(_, codons)| codons.contains(&codon))
            .map(|(name, _)| *name)
    }

    /// Group given RNA in vec by 3 chars 
    fn rna_to_codons(&self, rna: &str) -> Vec<String> {
        let mut rna_iter = rna.chars();
        (0..)
            .map(|_| rna_iter.by_ref().take(3).collect::<String>())
            .take_while(|s| !s.is_empty())
            .collect::<Vec<_>>()
    }

    pub fn of_rna(&self, rna: &str) -> Option<Vec<&'a str>> {
        self.rna_to_codons(rna)
            .iter()
            .map(|codon| self.name_for(codon))
            .take_while(|&s| s != Some("stop codon"))
            // actually Option<Vec<&str>>
            .collect::<Option<Vec<_>>>()
    }
}

pub fn parse<'a>(pairs: Vec<(&'a str, &'a str)>) -> CodonsInfo<'a> {
    let mut map: HashMap<&str, Vec<&str>> = HashMap::new();
    pairs
        .iter()
        .for_each(|(codon, name)| map.entry(name).or_insert(Vec::new()).push(codon));

    CodonsInfo(map)
}
