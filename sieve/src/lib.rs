pub fn primes_up_to(upper_bound: usize) -> Vec<usize> {
    let mut primes = vec![true; upper_bound + 1];
    (2..=upper_bound)
        .filter_map(|i| {
            if !primes[i] {
                return None;
            }
            (2..)
                .map(|j| i * j)
                .take_while(|num| *num <= upper_bound)
                .for_each(|num| primes[num] = false);
            Some(i)
        })
        .collect()
}
