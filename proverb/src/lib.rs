pub fn build_proverb(list: &[&str]) -> String {
    if list.is_empty() {
        return String::new();
    }
    list.windows(2)
        .map(|win| line(win[0], win[1]))
        .chain(std::iter::once(closing_line(list[0])))
        .collect::<Vec<_>>()
        .join("\n")
}

pub fn build_proverb_old(list: &[&str]) -> String {
    let length = list.len();
    if length == 0 {
        return String::new()
    }

    let mut proverb = String::with_capacity(200);

    if length > 1 {
        for i in 1..length {
            proverb.push_str(&line(list[i-1], list[i]));
        }
    }
    proverb.push_str(&closing_line(list[0]));
    
    proverb
}

fn line(a: &str, b: &str) -> String {
    format!("For want of a {} the {} was lost.", a, b)
}

fn closing_line(content: &str) -> String {
    format!("And all for the want of a {}.", content)
}
