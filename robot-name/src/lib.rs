use lazy_static::lazy_static;
use rand::{prelude, Rng};
use std::collections::HashSet;
use std::sync::Mutex;

lazy_static! {
    static ref USED: Mutex<HashSet<String>> = Mutex::new(HashSet::new());
}

pub struct Robot {
    name: String,
}

impl Default for Robot {
    fn default() -> Self {
        Robot::new()
    }
}

impl Robot {
    pub fn new() -> Self {
        Self {
            name: Robot::generate_name(),
        }
    }

    fn generate_name() -> String {
        let mut rng = rand::thread_rng();
        format!(
            "{}{}{}{}{}",
            Robot::rand_letter(&mut rng),
            Robot::rand_letter(&mut rng),
            Robot::rand_number(&mut rng),
            Robot::rand_number(&mut rng),
            Robot::rand_number(&mut rng)
        )
    }

    fn rand_letter(rng: &mut prelude::ThreadRng) -> char {
        (b'A' + rng.gen_range(0, 26)) as char
    }

    fn rand_number(rng: &mut prelude::ThreadRng) -> char {
        (b'0' + rng.gen_range(0, 10)) as char
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn reset_name(&mut self) {
        let mut new_name = Robot::generate_name();
        while !USED.lock().unwrap().insert(new_name.clone()) {
            new_name = Robot::generate_name();
        }
        self.name = new_name;
    }
}
