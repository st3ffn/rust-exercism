use std::io::{Read, Result, Write};

pub struct ReadStats<R> {
    delegate: R,
    bytes_through: usize,
    reads: usize,
}

impl<R: Read> ReadStats<R> {
    pub fn new(delegate: R) -> ReadStats<R> {
        ReadStats {
            delegate: delegate,
            bytes_through: 0,
            reads: 0,
        }
    }

    pub fn get_ref(&self) -> &R {
        &self.delegate
    }

    pub fn bytes_through(&self) -> usize {
        self.bytes_through
    }

    pub fn reads(&self) -> usize {
        self.reads
    }
}

impl<R: Read> Read for ReadStats<R> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.reads += 1;
        let bytes_read = self.delegate.read(buf)?;
        self.bytes_through += bytes_read;
        Ok(bytes_read)
    }
}

pub struct WriteStats<W> {
    delegate: W,
    bytes_through: usize,
    writes: usize,
}

impl<W: Write> WriteStats<W> {
    pub fn new(delegate: W) -> WriteStats<W> {
        WriteStats {
            delegate: delegate,
            bytes_through: 0,
            writes: 0,
        }
    }

    pub fn get_ref(&self) -> &W {
        &self.delegate
    }

    pub fn bytes_through(&self) -> usize {
        self.bytes_through
    }

    pub fn writes(&self) -> usize {
        self.writes
    }
}

impl<W: Write> Write for WriteStats<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.writes += 1;
        let bytes_written = self.delegate.write(buf)?;
        self.bytes_through += bytes_written;
        Ok(bytes_written)
    }

    fn flush(&mut self) -> Result<()> {
        self.delegate.flush()
    }
}
