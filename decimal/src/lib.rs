extern crate num_bigint;
use num_bigint::BigInt;
use std::cmp::Ordering;
use std::ops::{Add, Mul, Sub};

/// Type implementing arbitrary-precision decimal arithmetic
#[derive(Debug)]
pub struct Decimal {
    val: BigInt,
    decimals: usize,
}

impl Decimal {
    pub fn try_from(input: &str) -> Option<Decimal> {
        let mut s = input.splitn(2, '.');

        match (s.next(), s.next()) {
            (Some(pre_dec), Some(decimal)) => Some(Decimal {
                // BigInt implements trait std::str::FromStr
                val: [pre_dec, decimal].concat().parse().unwrap(),
                decimals: decimal.len(),
            }),
            (Some(pre_dec), None) => Some(Decimal {
                val: pre_dec.parse().unwrap(),
                decimals: 0,
            }),
            _ => None,
        }
    }

    fn normalize(&self, other: &Self) -> (BigInt, BigInt, usize) {
        let scale = (self.decimals as isize) - (other.decimals as isize);
        if scale >= 0 {
            // other is greater, so scale it
            let other_scaled = (0..scale).fold(other.val.clone(), |acc, _| acc * 10);
            (self.val.clone(), other_scaled, self.decimals)
        } else {
            // self is greater, so scale it
            let self_scaled = (0..scale.abs()).fold(self.val.clone(), |acc, _| acc * 10);
            (self_scaled, other.val.clone(), other.decimals)
        }
    }
}

impl Add for Decimal {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let (a, b, dec) = self.normalize(&other);
        Self {
            val: a + b,
            decimals: dec,
        }
    }
}

impl Sub for Decimal {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        let (a, b, dec) = self.normalize(&other);
        Self {
            val: a - b,
            decimals: dec,
        }
    }
}

impl Mul for Decimal {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        let (a, b, dec) = self.normalize(&other);
        Self {
            val: a * b,
            decimals: dec * 2,
        }
    }
}

impl PartialEq for Decimal {
    fn eq(&self, other: &Self) -> bool {
        let (a, b, _) = self.normalize(other);
        a == b
    }
}

impl PartialOrd for Decimal {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let (a, b, _) = self.normalize(other);
        a.partial_cmp(&b)
    }
}
