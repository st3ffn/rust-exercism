pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut res = Vec::new();
    let rows = input.len();

    for (row_idx, row) in input.iter().enumerate() {
        for (col_idx, value) in row.iter().enumerate() {
            if row.iter().all(|x| value >= x) &&
             (0..rows).all(|i| *value <= input[i][col_idx])
            {
                res.push((row_idx, col_idx));
            }
        }
    }

    res
}
