use std::collections::HashSet;

pub fn find(sum: u32) -> HashSet<[u32; 3]> {
    let mut res = HashSet::new();
    for a in 1..=sum / 3 {
        for b in a + 1..=(sum - a) / 2 {
            let c = sum - b - a;
            if a * a + b * b == c * c {
                res.insert([a, b, c]);
            }
        }
    }
    res
}
