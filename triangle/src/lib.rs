use std::cmp::Ordering;
use std::ops::Add;

pub struct Triangle<T>(T, T, T);

impl<T> Triangle<T>
where
    T: Copy + PartialOrd + Add<T, Output = T>,
{
    pub fn build(sides: [T; 3]) -> Option<Triangle<T>> {
        let mut sorted = sides;
        sorted.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));

        match sorted {
            // a + a == 0 means is a zero
            [a, _, _] if a + a == a => None,
            [a, b, c] if a + b >= c => Some(Triangle(a, b, c)),
            _ => None,
        }
    }

    pub fn is_equilateral(&self) -> bool {
        self.0 == self.1 && self.0 == self.2
    }

    pub fn is_scalene(&self) -> bool {
        self.0 != self.1 && self.0 != self.2 && self.1 != self.2
    }

    pub fn is_isosceles(&self) -> bool {
        self.0 == self.1 || self.0 == self.2 || self.1 == self.2
    }
}
