pub fn encode(source: &str) -> String {
    let mut res = String::new();
    let mut chars = source.chars().peekable();
    let mut count = 0;

    while let Some(c) = chars.next() {
        count += 1;
        if chars.peek() != Some(&c) {
            if count > 1 {
                res.push_str(&count.to_string());
            }
            res.push(c);
            count = 0;
        }
    }
    res
}

pub fn decode(source: &str) -> String {
    let mut res = String::new();
    let mut number_str = String::new();

    for c in source.chars() {
        if c.is_numeric() {
            number_str.push(c);
        } else {
            let number = number_str.parse().unwrap_or(1);
            res.push_str(&c.to_string().repeat(number));
            number_str.clear();
        }
    }

    res
}
