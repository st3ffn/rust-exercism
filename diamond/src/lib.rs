pub fn get_diamond(c: char) -> Vec<String> {
    let ai = alphabetic_index(c);
    (-ai..=ai).map(|row| {
        (-ai..=ai).map(|column| {
            if row.abs() + column.abs() == ai {
                alhabetic_char(column)
            } else {
                ' '
            }
        }).collect::<String>()
    }).collect()
}

/// convert alphabetic index to char A-Z where 0 is A
fn alhabetic_char(ai: i8) -> char {
    (ai.abs() as u8 + b'A') as char
}

/// alphabetic index from A-Z where A is 0
fn alphabetic_index(c: char) -> i8 {
    (c as u8 - b'A') as _
}
