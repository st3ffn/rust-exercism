use std::collections::BTreeMap;

#[derive(Default, Debug)]
pub struct School {
    db: BTreeMap<u32, Vec<String>>,
}

impl School {
    pub fn new() -> School {
        School {
            db: BTreeMap::new(),
        }
    }

    pub fn add(&mut self, grade: u32, student: &str) {
        let students: &mut Vec<String> = self.db.entry(grade).or_insert(Vec::new());
        students.push(student.to_string());
        students.sort();
    }

    pub fn grades(&self) -> Vec<u32> {
        self.db.keys().cloned().collect()
    }

    pub fn grade(&self, grade: u32) -> Option<Vec<String>> {
        self.db.get(&grade).map(|v| v.to_vec())
    }
}
