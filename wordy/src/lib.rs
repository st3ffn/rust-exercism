pub struct WordProblem;

pub fn answer(command: &str) -> Option<i32> {
    // not answerable
    if !command.starts_with("What is ") {
        return None;
    }

    let s = command.replace(" by", "");
    // use slice without "What is " and without last char which is a '?'
    let content = &s["What is ".len() - 1..s.len() - 1];

    // no content
    if content.is_empty() {
        return None;
    }

    let mut numbers = Vec::new();
    let mut ops: Vec<char> = Vec::new();
    let mut last_was_num = false;

    for s in content.split_whitespace() {
        if let Ok(x) = s.parse::<i32>() {
            if last_was_num {
                return None
            }
            numbers.push(x);
            last_was_num = true;
        } else if s.contains("plus") {
            ops.push('+');
            last_was_num = false;
        } else if s.contains("minus") {
            ops.push('-');
            last_was_num = false;
        } else if s.contains("multiplied") {
            ops.push('*');
            last_was_num = false;
        } else if s.contains("divided") {
            ops.push('/');
            last_was_num = false;
        } else {
            return None;
        }
    }

    if numbers.len() != ops.len() + 1 {
        return None
    }

    let mut res = numbers[0];
    let mut ops_itr = ops.iter();

    for x in numbers.into_iter().skip(1) {
        match ops_itr.next().unwrap() {
            '+' => res += x,
            '-' => res -= x,
            '*' => res *= x,
            '/' => res /= x,
            _ => {}
        };
    }
    Some(res)
}
